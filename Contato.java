package br.ucsal.bes20182.poo.atividade4.domain;

public class Contato {

	private String nome;

	private String telefone;

	private Integer anoNascimento;

	public Contato(String nome, String telefone, Integer anoNascimento) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoNascimento = anoNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

}
